package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;

public interface PostService {
    void createPost(Post post);
    void updatePost(Long id, Post post);
    void deletePost(Long id);
    Iterable<Post> getPosts();
}
